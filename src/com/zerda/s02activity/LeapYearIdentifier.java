package com.zerda.s02activity;

import java.util.Scanner;

public class LeapYearIdentifier {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);
        System.out.println("Please enter a year: \n");
        int year = Integer.parseInt(userInput.nextLine());

        String result1 = String.format("%d is a leap year", year);
        String result2 = String.format("%d is not a leap year", year);

        if (year % 4 == 0) {
            if (year % 100 == 0) {
                if (year%400 == 0) {
                    System.out.println(result1);
                } else {
                    System.out.println(result2);
                }
            } else {
                System.out.println(result1);
            }
        } else {
            System.out.println(result2);
        }
    }
}
